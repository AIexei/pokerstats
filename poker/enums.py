from enum import Enum


class Suit(Enum):
    bu = u'\u2666'
    pi = u'\u2660'
    kr = u'\u2663'
    ch = u'\u2665'


class Value(Enum):
    A = 'A', 13
    K = 'K', 12
    Q = 'Q', 11
    J = 'J', 10
    TEN = '10', 9
    NINE = '9', 8
    EIGHT = '8', 7
    SEVEN = '7', 6
    SIX = '6', 5
    FIVE = '5', 4
    FOUR = '4', 3
    THREE = '3', 2
    TWO = '2', 1
