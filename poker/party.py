import random
import copy

from poker.enums import Value, Suit


class Card:
    def __init__(self, suit: Suit, value: Value):
        self.suit = suit
        self.value = value

    @property
    def seniority(self):
        return self.value.value[1]

    def __str__(self):
        return self.value.value[0] + self.suit.value


CARDS = [Card(s, v) for s in Suit for v in Value]


class Party:
    def __init__(self, players_count: int, initial_cards: list=None):
        self.players_cards = [list() for _ in range(players_count)]
        self.table = []
        self.has_initial = bool(initial_cards)
        self.cards = copy.deepcopy(CARDS)

        if initial_cards:
            self.players_cards[0] = initial_cards

    def _get_card(self):
        index = random.randint(0, len(self.cards) - 1)
        card = self.cards[index]
        del self.cards[index]
        return card

    def _pass_on_table(self, count: int=3):
        self.initial()

        for _ in range(count):
            self.table.append(self._get_card())

    def initial(self):
        start = 1 if self.has_initial else 0
        for i in range(start, len(self.players_cards)):
            self.players_cards[i].extend([self._get_card(), self._get_card()])

    def flop(self):
        self._pass_on_table(3)

    def turn(self):
        self._pass_on_table(4)

    def river(self):
        self._pass_on_table(5)

    def __str__(self):
        result = 'Your cards: {}'.format(' '.join(map(str, self.players_cards[0])))

        for i in range(1, len(self.players_cards)):
            result += '\n{} player cards: {}'.format(i+1, ' '.join(map(str, self.players_cards[i])))

        result += '\nOn table: {}'.format(' '.join(map(str, self.table)))
        return result
